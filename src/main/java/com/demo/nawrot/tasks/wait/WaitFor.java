package com.demo.nawrot.tasks.wait;

/**
 * @author Rafal Nawrocki <rafal.nawrocki0@gmail.com>
 */
public class WaitFor {

    private WaitFor() {
    }

    public static OverlayInvisibility invisibleOverlay() {
        return new OverlayInvisibility();
    }
}
