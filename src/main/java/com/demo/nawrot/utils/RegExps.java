package com.demo.nawrot.utils;

/**
 * @author Rafal Nawrocki <rafal.nawrocki0@gmail.com>
 */
public class RegExps {

    public static String TRIMMED_OVERALL_PRICE_REGEXP = "(\\d+),00zł$";

    private RegExps() {
    }
}
