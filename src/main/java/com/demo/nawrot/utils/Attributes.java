package com.demo.nawrot.utils;

/**
 * @author Rafal Nawrocki <rafal.nawrocki0@gmail.com>
 */
public class Attributes {

    public static final String PRODUCT_PRICE = "data-product-price";

    private Attributes() {
    }
}
